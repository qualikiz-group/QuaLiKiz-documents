This repository contains documents related to QuaLiKiz. Currently under construction.

# Conference papers
- [A test document] (conferences/test_document_pdf.pdf)

## The Something conference
- [Another test document] (conferences/something/test_document_pdf_something.pdf)

# Journal publications
- [More tests!] (journal_publications/test_document_pdf.pdf)

# Reports
- [And even more tests] (reports/test_document_pdf.pdf)
